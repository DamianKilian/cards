<?php
Route::get('/test', 'testController@index');
Route::get('/dbutilTest', 'testController@dbutil');
Route::get('/_clearcache', 'UtilController@_clearcache');
Route::get('/_migr', 'UtilController@_migr');

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

//Route::get('/', 'HomeController@welcome')->name('welcome');
Route::get('/', 'HomeController@index')->name('home');
Route::post('/updatedb', 'HomeController@updatedb')->name('updatedb');
Route::get('/adduser', 'Auth\AddUserController@showRegistrationForm')->name('adduser');
Route::post('/adduser', 'Auth\AddUserController@adduser');

//DataTables
Route::post('/datatables', 'DataTables\DtController@dataTables')->name('datatables');
Route::post('/dt-categories', 'DataTables\DtController@categories')->name('dt-categories');
Route::post('/dt-datetimepicker', 'DataTables\DtController@datetimepicker')->name('dt-datetimepicker');

Auth::routes(['register' => false]);
