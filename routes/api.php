<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::post('login', 'API\LoginController@login');

Route::resource('producers', 'API\ProducerController')->only([
    'index',
]);

Route::resource('clients', 'API\ClientController')->only([
    'index',
]);

Route::resource('categories', 'API\CategoryController')->only([
    'index',
]);

Route::resource('products', 'API\ProductController')->only([
    'index',
]);

Route::resource('orders', 'API\OrderController')->only([
    'index', 'show', 'store', 'update', 'destroy',
]);

Route::post('newpackage', 'API\ApiController@newpackage');



