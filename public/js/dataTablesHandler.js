"use strict";

window.dth = {
	token: $('meta[name="csrf-token"]').attr('content'),
	tables:{
		orders:{
			el: $('#dt-orders'),
			details: function(d){
				var table = $('<div class="detailsContent table-dark">'
					+'<br><div class="client" style="text-align:center"> Klient: <b></b></div>'
					+'<div class="price_products" style="text-align:center"> Cena wszystkich produktów: <b></b></div><br>'
					+'<table class="table table-dark">'
					+'<thead><tr>'
						+'<th scope="col">#</th>'
						+'<th scope="col">Produkt</th>'
						+'<th scope="col">Opis</th>'
						+'<th scope="col">Cena</th>'
						+'<th scope="col">Ilość</th>'
						+'<th scope="col">Cena suma</th>'
						+'<th scope="col">Komentarz</th>'
					+'</thead>'
					+'<tbody><tr class="trEmpty">'
						+'<th scope="row"></th>'
						+'<td class="name"></td>'
						+'<td class="desc"></td>'
						+'<td class="price"></td>'
						+'<td class="number"></td>'
						+'<td class="price_sum"></td>'
						+'<td class="comment"></td>'
					+'</tr></tbody>'
					+'</table></div>');
					
				table.find('.client b').html(d.client+', '+d.client_surname);
				table.find('.price_products b').html(d.price_products);
				var tbody = table.find('table tbody');
				var trEmpty = tbody.find( '.trEmpty' ).detach();
				var x = 1;
				$.each(d.products, function(){
					var tr = trEmpty.clone();
					tr.children('th').html(x++);
					$.each(this, function(i, v){
						tr.children('.'+i).html(v);
					});
					tbody.append(tr);
				});
				return table;
			},
			db: { with: ['client', 'products'] },
			model: 'Order',
			columns: [
				{ class: "details-control", orderable: false, data: null,
					defaultContent: ' <span class="badge badge-primary text-wrap">+</span> '
				},
				{ data: "client" },
				{ data: "comment" },
				{ data: "created_at" },
				{ data: "updated_at" },
				{ data: "payment_term" },
				{ data: "sent" },
				{ data: "pdf", defaultContent: ' <span class="badge badge-primary text-wrap"><i class="fas fa-file-pdf"></i></span> '}
			]
		},
		products:{
			el: $('#dt-products'),
			db: { with: ['producer', 'category', 'details', 'photos'] },
			model: 'Product',
			columns: [
				{ data: "name" },
				{ data: "category" },
				{ data: "producer" },
				{ data: "desc" },
				{ data: "photos" },
				{ data: "updated_at" },
				{ data: "price" },
				{ data: "visible" }
			]
		},
		categories:{
			el: $('#dt-categories'),
		},
		rabates:{
			el: $('#dt-rabates'),
			details: function(d){
				var table = $('<div class="detailsContent table-dark">'
					+'<br><div class="client" style="text-align:center"> Klient: <b></b></div>'
					+'<table class="table table-dark">'
					+'<thead><tr>'
						+'<th scope="col">#</th>'
						+'<th scope="col">Producent</th>'
						+'<th scope="col">Rabat</th>'
					+'</thead>'
					+'<tbody><tr class="trEmpty">'
						+'<th scope="row"></th>'
						+'<td class="producer"></td>'
						+'<td class="rabate"></td>'
					+'</tr></tbody>'
					+'</table></div>');
				table.find('.client b').html(d.client+', '+d.client_surname);
				var tbody = table.find('table tbody');
				var trEmpty = tbody.find( '.trEmpty' ).detach();
				var x = 1;
				$.each(d.producerRabates, function(){
					var tr = trEmpty.clone();
					tr.children('th').html(x++);
					$.each(this, function(i, v){
						tr.children('.'+i).html(v);
					});
					tbody.append(tr);
				});
				return table;
			},
			db: { with: 'producerRabates.Producer'},
			model: 'Client',
			resource: 'ClientRabate',
			columns: [
				{ class: "details-control", orderable: false, data: null,
					defaultContent: ' <span class="badge badge-primary text-wrap">+</span> '
				},
				{ data: "client" },
				{ data: "rabate" }
			]
		},
		producers:{
			el: $('#dt-producers'),
			model: 'Producer',
			columns: [
				{ data: "id" },
				{ data: "name" }
			]
		},
		clients:{
			el: $('#dt-clients'),
			model: 'Client',
			createdRow: function(row, data) {
				var row = $(row);
				var date = row.find('.date');
				date.datetimepicker({
					defaultDate: data.payment_termDate,
					format: 'YYYY-MM-DD HH:mm:ss'
				});
			},
			columns: [
				{ data: "id" },
				{ data: "name" },
				{ data: "address" },
				{ data: "rabate" },
				{ data: "payment_term",
					defaultContent: ''
						+'<form>'
							+'<div class="form-group" style="margin:0;display:inline-block">'
								+'<div class="input-group date">'
									+'<div class="input-group-prepend input-group-addon">'
										+'<span class="input-group-text" id="addon-wrapping"><i class="fas fa-calendar-day"></i></span>'
									+'</div>'
									+'<input type="text" class="form-control">'
								+'</div>'
							+'</div>'
							+'<button type="button" class="btn btn-link save"> Zapisz</button>'
							+'<div class="spinner-border text-primary loader" role="status" style="display: none;">'
								+'<span class="sr-only">Loading...</span>'
							+'</div>'
						+'</form>'
				},
				{ data: "payment_reception" }
			]
		},
		representatives:{
			el: $('#dt-representatives'),
			details: function(d){
				var table = $('<div class="detailsContent table-dark">'
					+'<table class="table table-dark">'
					+'<thead><tr>'
						+'<th scope="col">#</th>'
						+'<th scope="col">Klient</th>'
					+'</thead>'
					+'<tbody><tr class="trEmpty">'
						+'<th scope="row"></th>'
						+'<td class="client"></td>'
					+'</tr></tbody>'
					+'</table></div>');
				var tbody = table.find('table tbody');
				var trEmpty = tbody.find( '.trEmpty' ).detach();
				var x = 1;
				$.each(d.clients, function(){
					var tr = trEmpty.clone();
					tr.children('th').html(x++);
					$.each(this, function(i, v){
						tr.children('.'+i).html(v);
					});
					tbody.append(tr);
				});
				return table;
			},
			db: { with: 'clients' },
			model: 'Representative',
			columns: [
				{ class: "details-control", orderable: false, data: null,
					defaultContent: ' <span class="badge badge-primary text-wrap">+</span> '
				},
				{ data: "id" },
				{ data: "name" }
			]
		}
	},
	init: function(){
		dth.events();
		dth.loadTable('orders');
	},
	loadTable: function (table) {
		dth.config(table);
		if(this.details){
			dth.details(table);
		}
		if(this.datetimepicker){
			dth.datetimepicker(table);
		}
	},
	datetimepicker: function(table){
		dth.tables[table].el.find('tbody').on('click', 'tr td .save', function () {
			var btn = $(this);
			btn.hide();
			var form = btn.closest('form');
			var loader = form.find('.loader');
			loader.show();
			var tr = btn.closest('tr');
			var jqXHR = $.post( btn.closest('#nav-tabContent').data('url')+'/dt-datetimepicker', {
				_token: dth.token,
				model: dth.tables[table].model,
				id: tr.attr('id'),
				date: btn.closest('form').find('.date input').val()
			}, 'json');
			jqXHR.done(function( msg ) {
				if(msg.ok){
					btn.show();
					loader.hide();
					tr.find('.reception').html(msg.reception);
				}else{
					form.html('błąd')
				}
			});
			jqXHR.fail(function( msg, textStatus) {
				form.html(textStatus)
			});
		});
	},
	details: function(table){
		var dt = dth.tables[table].dt;
		// Array to track the ids of the details displayed rows
		var detailRows = [];
		dth.tables[table].el.find('tbody').on( 'click', 'tr td.details-control', function () {
			var tr = $(this).closest('tr');
			var row = dt.row( tr );
			var idx = $.inArray( tr.attr('id'), detailRows );
			if ( row.child.isShown() ) {
				tr.removeClass( 'details' );
				row.child.hide();
				// Remove from the 'open' array
				detailRows.splice( idx, 1 );
			}
			else {
				tr.addClass( 'details' );
				row.child( dth.tables[table].details(row.data()) ).show();
				// Add to the 'open' array
				if ( idx === -1 ) {
					detailRows.push( tr.attr('id') );
				}
			}
		} );
		// On each draw, loop over the `detailRows` array and show any child rows
		dt.on( 'draw', function () {
			$.each( detailRows, function ( i, id ) {
				$('#'+id+' td.details-control').trigger( 'click' );
			} );
		} );
	},
	config: function(table){
		dth.tables[table].dt = dth.tables[table].el
		.on('preXhr.dt', function ( e, settings, data ) {
			data._token = dth.token;
		})
		.DataTable({
			processing: true,
			serverSide: true,
			createdRow: function(row, data) {
				if(dth.tables[table].createdRow){
					dth.tables[table].createdRow(row, data);
				}
			},
			ajax: {
				url: dth.tables[table].el.closest('#nav-tabContent').data('url')+'/datatables',
				type: 'POST',
				data: {
					db: dth.tables[table].db,
					model: dth.tables[table].model,
					resource: dth.tables[table].resource
				}
			},
			columns: dth.tables[table].columns
		});
	},
	loadTree: function (table) {
		$.ajax({
			url: dth.tables[table].el.closest('#nav-tabContent').data('url')+'/dt-'+table,
			method: 'POST',
			dataType: 'json',
			data:{
				_token: dth.token
			}
		}).done(function (msg) {
			dth.tables[table].el.find('.loader').remove();
			dth.tables[table].el.find('.d-none').removeClass('d-none');
			var searchableTree = dth.tables[table].el.find('#tree').treeview({
				data: msg.tree,
				levels: 1,
				expandIcon: 'fas fa-folder-plus',
				collapseIcon: 'fas fa-folder-minus',
			});
			var searchCatTree = dth.tables[table].el.find('#searchCatTree');
			var search = function (e) {
				var pattern = searchCatTree.val();
				var options = {
					ignoreCase: true,
					exactMatch: false,
					revealResults: true
				};
				searchableTree.treeview('search', [pattern, options]);
			}
			dth.tables[table].el.find('#btn-search').on('click', search);
			dth.tables[table].el.find('#btn-clear-search').on('click', function (e) {
				searchableTree.treeview('clearSearch');
				searchCatTree.val('');
				searchableTree.treeview('collapseAll');
			});
			
			
		}).fail(function (jqXHR, textStatus) {
			alert("Request failed: " + textStatus);
		});
	},
	events: function(){
		$('#dt-nav a').not('.tree, .active').one('click', function(){
			var table = $(this).data('table');
			dth.loadTable(table);
		});
		$('#dt-nav .tree').one('click', function(){
			var table = $(this).data('table');
			dth.loadTree(table);
		});
	}
};

dth.init();


