"use strict";

window.app = {
	init: function(){
		app.events();
	},
	events: function(){
		$('#updateDb').on('click', function(){
			var btn = $(this);
			btn.hide();
			var lastUpdateWrapper = btn.closest('#lastUpdateWrapper');
			var lastUpdate = lastUpdateWrapper.find('#lastUpdate');
			var newPackageDate = lastUpdateWrapper.find('#newPackageDate');
			var spinner = lastUpdateWrapper.find('.spinner-border');
			spinner.show();
			var jqXHR = $.post( btn.data('url'), {_token: $('meta[name="csrf-token"]').attr('content')}, "json");
			jqXHR.done(function( msg ) {
				if(true === msg.updated){
					spinner.hide();
					btn.show();
					lastUpdate.html(msg.date);
					newPackageDate.html(msg.newPackage);
				}
			});
			jqXHR.fail(function( jqXHR, textStatus, error ) {
				spinner.hide();
				btn.show();
				lastUpdate.html(error);
			});
		});
	}
};

app.init();

