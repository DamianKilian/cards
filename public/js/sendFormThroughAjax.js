"use strict";

window.sendFormThroughAjax = {
	form: $('.sendFormThroughAjax'),
	successAlert: $('<div class="alert alert-success" role="alert">użytkownik dodany</div>'),
	send: function(){
		sendFormThroughAjax.form.submit(function( e ) {
			e.preventDefault();
			$('.is-invalid').removeClass('is-invalid');
			$('.invalid-feedback').remove();
			var jqXHR = $.post( sendFormThroughAjax.form.attr( "action" ), sendFormThroughAjax.form.serializeArray(), "json");
			jqXHR.done(function( msg ) {
				if(msg.success){
					sendFormThroughAjax.form.parent().prepend(sendFormThroughAjax.successAlert);
					sendFormThroughAjax.successAlert.fadeOut(2000);
				}else{
					sendFormThroughAjax.form.parent().prepend('<div class="alert alert-warning" role="alert">'+msg.msg+'</div>');
				}
			});
			jqXHR.fail(function( msg ) {
				msg = msg.responseJSON;
				if('undefined' !== typeof msg.errors){
					$.each(msg.errors, function(i, v){
						var input = sendFormThroughAjax.form.find('#'+i);
						input.addClass('is-invalid');
						input.parent().append('<span class="invalid-feedback" role="alert"><strong>'+ v +'</strong></span>');
					});
				}
			});
		});
	},
};

sendFormThroughAjax.send();






