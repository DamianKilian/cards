<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProducerRabatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('producer_rabates', function (Blueprint $table) {
            $table->increments('id');
			$table->unsignedInteger('producer_rabate');
            $table->timestamps();

			$table->unsignedInteger('client_id');
			$table->unsignedInteger('producer_id');
            $table->foreign('client_id')
                  ->references('id')->on('clients')
                  ->onDelete('cascade');
            $table->foreign('producer_id')
                  ->references('id')->on('producers')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('producer_rabates');
    }
}
