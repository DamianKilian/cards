<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
			$table->string('name');
			$table->text('desc')->nullable();
			$table->text('photos')->nullable();
            $table->timestamps();
			$table->unsignedInteger('category_id');
			$table->unsignedInteger('producer_id');
			$table->foreign('category_id')
				  ->references('id')->on('categories');
			$table->foreign('producer_id')
				  ->references('id')->on('producers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
