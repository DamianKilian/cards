<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Metadata extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('meta_data', function (Blueprint $table) {
			$table->string('table_name')->nullable();
        });
        Schema::table('db_updates', function (Blueprint $table) {
			$table->dateTime('new_package')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('meta_data', function($table) {
		   $table->dropColumn('table_name');
		});
		Schema::table('db_updates', function($table) {
		   $table->dropColumn('new_package');
		});
    }
}
