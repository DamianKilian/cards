<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OrderProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_product', function (Blueprint $table) {
			$table->unsignedInteger('order_id');
			$table->unsignedBigInteger('product_id');
			$table->integer('number')->unsigned()->default(1);
			$table->timestamps();

			$table->foreign('order_id')->references('id')->on('orders')
				->onDelete('cascade');
			$table->foreign('product_id')->references('id')->on('products')
				->onDelete('cascade');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_product');
    }
}
