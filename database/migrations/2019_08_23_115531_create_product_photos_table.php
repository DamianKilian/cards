<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductPhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_photos', function (Blueprint $table) {
			$table->unsignedBigInteger('product_id');
			$table->integer('photo_nr')->unsigned();
			$table->integer('position')->unsigned();
			$table->text('desc');
			$table->timestamps();

			$table->foreign('product_id')->references('id')->on('products')
				->onDelete('cascade');
        });
		Schema::table('products', function($table) {
		   $table->dropColumn('photos');
		});
		Schema::table('product_details', function (Blueprint $table) {
			$table->integer('photo_nr')->nullable();
			$table->string('photo_dir')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_photos');
		Schema::table('products', function (Blueprint $table) {
			$table->text('photos')->nullable();
        });
		Schema::table('product_details', function($table) {
		   $table->dropColumn('photo_nr');
		   $table->dropColumn('photo_dir');
		});
    }
}
