<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateOrdersAndOrderProductTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
			$table->boolean('sent');
			$table->text('pdf')->nullable();
			$table->text('payment_term')->nullable();
        });

        Schema::table('order_product', function (Blueprint $table) {
			$table->text('comment');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('orders', function($table) {
		   $table->dropColumn('sent');
		   $table->dropColumn('pdf');
		   $table->dropColumn('payment_term');
		});
		Schema::table('order_product', function($table) {
		   $table->dropColumn('comment');
		});
    }
}
