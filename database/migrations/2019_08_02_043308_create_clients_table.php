<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');
			$table->string('phone')->nullable();
			$table->string('email')->nullable();
			$table->string('name');
			$table->string('surname')->nullable();
			$table->string('company')->nullable();
			$table->string('city')->nullable();
			$table->string('code')->nullable();
			$table->string('address')->nullable();
			$table->integer('rabate')->default(0);
			$table->string('active')->default('');
            $table->timestamps();

			$table->unsignedInteger('representative_id')->nullable();
            $table->foreign('representative_id')
                  ->references('id')->on('representatives');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
