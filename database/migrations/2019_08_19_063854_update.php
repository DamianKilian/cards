<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Update extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_details', function (Blueprint $table) {
			$table->bigIncrements('id');
        });
        Schema::table('products', function (Blueprint $table) {
			$table->decimal('price', 9, 2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('product_details', function($table) {
		   $table->dropColumn('id');
		});
        Schema::table('products', function (Blueprint $table) {
			$table->dropColumn('price');
        });
    }
}
