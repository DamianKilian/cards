<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_details', function (Blueprint $table) {
			$table->unsignedBigInteger('product_id');
			$table->unsignedInteger('vat');

			$table->foreign('product_id')->references('id')->on('products')
				->onDelete('cascade');
        });
        Schema::table('products', function (Blueprint $table) {
			$table->boolean('visible')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_details');
		Schema::table('products', function($table) {
		   $table->dropColumn('visible');
		});
    }
}
