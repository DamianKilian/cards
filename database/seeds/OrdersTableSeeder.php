<?php

use Illuminate\Database\Seeder;

class OrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('orders')->insert([
			[
				'comment' => 'order_comment_1', 'client_id' => '2',
			],
			[
				'comment' => 'order_comment_2', 'client_id' => '3',
			],
			[
				'comment' => 'order_comment_3', 'client_id' => '4',
			],
			[
				'comment' => 'order_comment_4', 'client_id' => '5',
			],
			[
				'comment' => 'order_comment_5', 'client_id' => '6',
			],
			[
				'comment' => 'order_comment_6', 'client_id' => '7',
			],
			[
				'comment' => 'order_comment_7', 'client_id' => '8',
			],
			[
				'comment' => 'order_comment_8', 'client_id' => '9',
			],
			[
				'comment' => 'order_comment_9', 'client_id' => '10',
			],
			[
				'comment' => 'order_comment_10', 'client_id' => '11',
			],
			[
				'comment' => 'order_comment_11', 'client_id' => '1',
			],
			[
				'comment' => 'order_comment_12', 'client_id' => '2',
			],
			[
				'comment' => 'order_comment_13', 'client_id' => '3',
			],
			[
				'comment' => 'order_comment_14', 'client_id' => '4',
			],
			[
				'comment' => 'order_comment_15', 'client_id' => '5',
			],
			[
				'comment' => 'order_comment_16', 'client_id' => '6',
			],
			[
				'comment' => 'order_comment_17', 'client_id' => '7',
			],
			[
				'comment' => 'order_comment_18', 'client_id' => '8',
			],
			[
				'comment' => 'order_comment_19', 'client_id' => '9',
			],
			[
				'comment' => 'order_comment_20', 'client_id' => '21',
			],
			[
				'comment' => 'order_comment_21', 'client_id' => '22',
			],
			[
				'comment' => 'order_comment_22', 'client_id' => '23',
			],
			[
				'comment' => 'order_comment_23', 'client_id' => '24',
			],
			[
				'comment' => 'order_comment_24', 'client_id' => '25',
			],
			[
				'comment' => 'order_comment_25', 'client_id' => '26',
			],
			[
				'comment' => 'order_comment_26', 'client_id' => '27',
			],
			[
				'comment' => 'order_comment_27', 'client_id' => '28',
			],
			[
				'comment' => 'order_comment_28', 'client_id' => '29',
			],
			[
				'comment' => 'order_comment_29', 'client_id' => '31',
			],
			[
				'comment' => 'order_comment_30', 'client_id' => '31',
			],
			[
				'comment' => 'order_comment_31', 'client_id' => '31',
			],
			[
				'comment' => 'order_comment_32', 'client_id' => '32',
			],
			[
				'comment' => 'order_comment_33', 'client_id' => '32',
			],
			[
				'comment' => 'order_comment_34', 'client_id' => '32',
			],
			[
				'comment' => 'order_comment_35', 'client_id' => '33',
			],
			[
				'comment' => 'order_comment_36', 'client_id' => '33',
			],
			[
				'comment' => 'order_comment_37', 'client_id' => '33',
			],
			[
				'comment' => 'order_comment_38', 'client_id' => '34',
			],
			[
				'comment' => 'order_comment_39', 'client_id' => '35',
			],
			[
				'comment' => 'order_comment_40', 'client_id' => '36',
			],
			[
				'comment' => 'order_comment_41', 'client_id' => '37',
			],
			[
				'comment' => 'order_comment_42', 'client_id' => '38',
			],
			[
				'comment' => 'order_comment_43', 'client_id' => '39',
			],
			[
				'comment' => 'order_comment_44', 'client_id' => '30',
			],
        ]);
    }
}
