<?php

use Illuminate\Database\Seeder;
use App\Client;

class ClientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		for ($id = 1; $id <= 24; $id++) {
			$client = Client::find($id);
			$client->representative_id = $id+1;
			$client->save();
		}
    }
}
