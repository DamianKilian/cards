<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
			[
				'name' => 'admin',
				'email' => 'admin@admin.admin',
				'password' => bcrypt('admin'),
				'api_token' => bin2hex(openssl_random_pseudo_bytes(30)),
			],
			[
				'name' => 'test',
				'email' => 'test@test.test',
				'password' => bcrypt('test'),
				'api_token' => bin2hex(openssl_random_pseudo_bytes(30)),
			],
        ]);
    }
}
