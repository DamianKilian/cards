<?php

use Illuminate\Database\Seeder;

class RepresentativesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('representatives')->insert([
			[
				'name' => 'przedstawiciel_1',
			],
			[
				'name' => 'przedstawiciel_2',
			],
			[
				'name' => 'przedstawiciel_3',
			],
			[
				'name' => 'przedstawiciel_4',
			],
			[
				'name' => 'przedstawiciel_5',
			],
			[
				'name' => 'przedstawiciel_6',
			],
			[
				'name' => 'przedstawiciel_7',
			],
			[
				'name' => 'przedstawiciel_8',
			],
			[
				'name' => 'przedstawiciel_9',
			],
			[
				'name' => 'przedstawiciel_10',
			],
			[
				'name' => 'przedstawiciel_11',
			],
			[
				'name' => 'przedstawiciel_12',
			],
			[
				'name' => 'przedstawiciel_13',
			],
			[
				'name' => 'przedstawiciel_14',
			],
			[
				'name' => 'przedstawiciel_15',
			],
			[
				'name' => 'przedstawiciel_16',
			],
			[
				'name' => 'przedstawiciel_17',
			],
			[
				'name' => 'przedstawiciel_18',
			],
			[
				'name' => 'przedstawiciel_19',
			],
			[
				'name' => 'przedstawiciel_20',
			],
			[
				'name' => 'przedstawiciel_21',
			],
			[
				'name' => 'przedstawiciel_22',
			],
			[
				'name' => 'przedstawiciel_23',
			],
			[
				'name' => 'przedstawiciel_24',
			],
			[
				'name' => 'przedstawiciel_25',
			],
			[
				'name' => 'przedstawiciel_26',
			],
			[
				'name' => 'przedstawiciel_27',
			],
			[
				'name' => 'przedstawiciel_28',
			],
			[
				'name' => 'przedstawiciel_29',
			],
			[
				'name' => 'przedstawiciel_30',
			],
			[
				'name' => 'przedstawiciel_31',
			],
			[
				'name' => 'przedstawiciel_32',
			],
			[
				'name' => 'przedstawiciel_33',
			],
			[
				'name' => 'przedstawiciel_34',
			],
			[
				'name' => 'przedstawiciel_35',
			],
			[
				'name' => 'przedstawiciel_36',
			],
			[
				'name' => 'przedstawiciel_37',
			],
			[
				'name' => 'przedstawiciel_38',
			],
			[
				'name' => 'przedstawiciel_39',
			],
			[
				'name' => 'przedstawiciel_40',
			],
			[
				'name' => 'przedstawiciel_41',
			],
			[
				'name' => 'przedstawiciel_42',
			],
			[
				'name' => 'przedstawiciel_43',
			],
			[
				'name' => 'przedstawiciel_44',
			],
        ]);
    }
}
