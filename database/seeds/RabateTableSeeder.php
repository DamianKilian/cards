<?php

use Illuminate\Database\Seeder;

class RabateTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('producer_rabates')->insert([
			[
				'producer_rabate' => '1', 'client_id' => '2', 'producer_id' => '1',
			],
			[
				'producer_rabate' => '5', 'client_id' => '3', 'producer_id' => '1',
			],
			[
				'producer_rabate' => '5', 'client_id' => '4', 'producer_id' => '1',
			],
			[
				'producer_rabate' => '4', 'client_id' => '5', 'producer_id' => '1',
			],
			[
				'producer_rabate' => '5', 'client_id' => '6', 'producer_id' => '3',
			],
			[
				'producer_rabate' => '6', 'client_id' => '7', 'producer_id' => '3',
			],
			[
				'producer_rabate' => '7', 'client_id' => '8', 'producer_id' => '3',
			],
			[
				'producer_rabate' => '8', 'client_id' => '9', 'producer_id' => '3',
			],
			[
				'producer_rabate' => '9', 'client_id' => '10', 'producer_id' => '3',
			],
			[
				'producer_rabate' => '10', 'client_id' => '11', 'producer_id' => '3',
			],
			[
				'producer_rabate' => '11', 'client_id' => '1', 'producer_id' => '3',
			],
			[
				'producer_rabate' => '12', 'client_id' => '2', 'producer_id' => '3',
			],
			[
				'producer_rabate' => '13', 'client_id' => '3', 'producer_id' => '4',
			],
			[
				'producer_rabate' => '14', 'client_id' => '4', 'producer_id' => '4',
			],
			[
				'producer_rabate' => '15', 'client_id' => '5', 'producer_id' => '4',
			],
			[
				'producer_rabate' => '16', 'client_id' => '6', 'producer_id' => '4',
			],
			[
				'producer_rabate' => '17', 'client_id' => '7', 'producer_id' => '4',
			],
			[
				'producer_rabate' => '18', 'client_id' => '8', 'producer_id' => '5',
			],
			[
				'producer_rabate' => '19', 'client_id' => '9', 'producer_id' => '5',
			],
			[
				'producer_rabate' => '0', 'client_id' => '21', 'producer_id' => '5',
			],
			[
				'producer_rabate' => '1', 'client_id' => '22', 'producer_id' => '5',
			],
			[
				'producer_rabate' => '2', 'client_id' => '23', 'producer_id' => '5',
			],
			[
				'producer_rabate' => '5', 'client_id' => '24', 'producer_id' => '7',
			],
			[
				'producer_rabate' => '4', 'client_id' => '25', 'producer_id' => '7',
			],
			[
				'producer_rabate' => '5', 'client_id' => '26', 'producer_id' => '7',
			],
			[
				'producer_rabate' => '6', 'client_id' => '27', 'producer_id' => '7',
			],
			[
				'producer_rabate' => '7', 'client_id' => '28', 'producer_id' => '7',
			],
			[
				'producer_rabate' => '8', 'client_id' => '29', 'producer_id' => '7',
			],
			[
				'producer_rabate' => '9', 'client_id' => '31', 'producer_id' => '7',
			],
			[
				'producer_rabate' => '0', 'client_id' => '31', 'producer_id' => '7',
			],
			[
				'producer_rabate' => '1', 'client_id' => '31', 'producer_id' => '8',
			],
			[
				'producer_rabate' => '2', 'client_id' => '32', 'producer_id' => '8',
			],
			[
				'producer_rabate' => '3', 'client_id' => '32', 'producer_id' => '8',
			],
			[
				'producer_rabate' => '4', 'client_id' => '32', 'producer_id' => '8',
			],
			[
				'producer_rabate' => '5', 'client_id' => '33', 'producer_id' => '8',
			],
			[
				'producer_rabate' => '6', 'client_id' => '33', 'producer_id' => '9',
			],
			[
				'producer_rabate' => '7', 'client_id' => '33', 'producer_id' => '9',
			],
			[
				'producer_rabate' => '8', 'client_id' => '34', 'producer_id' => '9',
			],
			[
				'producer_rabate' => '9', 'client_id' => '35', 'producer_id' => '9',
			],
        ]);
    }
}
