<?php

use Illuminate\Database\Seeder;

class OrderProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('order_product')->insert([
			[
				'order_id' => '1','product_id' => '1','number' => '3',
			],
			[
				'order_id' => '1','product_id' => '334','number' => '3',
			],
			[
				'order_id' => '1','product_id' => '335','number' => '3',
			],
			[
				'order_id' => '2','product_id' => '299','number' => '5',
			],
			[
				'order_id' => '2','product_id' => '337','number' => '5',
			],
			[
				'order_id' => '2','product_id' => '338','number' => '5',
			],
			[
				'order_id' => '2','product_id' => '339','number' => '5',
			],
			[
				'order_id' => '3','product_id' => '300','number' => '4',
			],
			[
				'order_id' => '4','product_id' => '304','number' => '7',
			],
			[
				'order_id' => '5','product_id' => '305','number' => '6',
			],
			[
				'order_id' => '6','product_id' => '306','number' => '1',
			],
			[
				'order_id' => '7','product_id' => '307','number' => '7',
			],
			[
				'order_id' => '8','product_id' => '308','number' => '8',
			],
			[
				'order_id' => '9','product_id' => '309','number' => '1',
			],
			[
				'order_id' => '10','product_id' => '310','number' => '9',
			],
			[
				'order_id' => '11','product_id' => '311','number' => '1',
			],
			[
				'order_id' => '12','product_id' => '301','number' => '1',
			],
			[
				'order_id' => '13','product_id' => '302','number' => '1',
			],
			[
				'order_id' => '14','product_id' => '303','number' => '2',
			],
			[
				'order_id' => '15','product_id' => '328','number' => '3',
			],
			[
				'order_id' => '16','product_id' => '329','number' => '4',
			],
			[
				'order_id' => '17','product_id' => '332','number' => '1',
			],
			[
				'order_id' => '18','product_id' => '340','number' => '1',
			],
			[
				'order_id' => '19','product_id' => '341','number' => '1',
			],
			[
				'order_id' => '20','product_id' => '342','number' => '1',
			],
			[
				'order_id' => '21','product_id' => '343','number' => '1',
			],
			[
				'order_id' => '22','product_id' => '344','number' => '1',
			],
			[
				'order_id' => '23','product_id' => '345','number' => '1',
			],
			[
				'order_id' => '24','product_id' => '346','number' => '1',
			],
			[
				'order_id' => '25','product_id' => '348','number' => '1',
			],
			[
				'order_id' => '26','product_id' => '349','number' => '1',
			],
        ]);
    }
}
