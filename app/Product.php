<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function producer()
    {
        return $this->belongsTo('App\Producer');
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function details()
    {
        return $this->hasOne('App\ProductDetails');
    }

    public function photos()
    {
        return $this->hasMany('App\ProductPhotos');
    }

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'photos' => 'array',
    ];
}
