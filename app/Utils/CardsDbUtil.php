<?php

namespace App\Utils;

use App\MetaData;
use Illuminate\Support\Facades\DB;

class CardsDbUtil {

	//Selly tables in correct order to update, and mappind
    private $sellyTables = [
		'producenci' => array(
			'id' => 'producent_id',
			'name' => 'nazwa',
		),
		'kategorie' => array(
			'id' => 'kategoria_id',
			'name' => 'nazwa',
			'parent_id' => 'parent_id',
		),
		'produkty' => array(
			'id' => 'product_id',
			'name' => 'product_name',
//			'desc' => '',
//			'photos' => '',
			'category_id' => 'category_id',
			'producer_id' => 'producer_id',
			'visible' => 'visible',
			'price' => 'price'
		),
		'klienci' => array(
			'id' => 'klient_id',
			'phone' => 'telefon',
			'email' => 'email',
			'name' => 'f_imie',
			'surname' => 'f_nazwisko',
			'company' => 'f_firma',
			'city' => 'f_miasto',
			'code' => 'f_kod',
			'address' => 'f_adres',
			'rabate' => 'rabat_klienta',
			'active' => 'active',
//			'representative_id' => '',
		),
		'produkty_wlasciwosci' => array(
			'product_id' => 'produkt_id',
			'vat' => 'stawka_vat',
			'photo_nr' => 'zdjecie_nr',
			'photo_dir' => 'zdjecia_katalog',
		),
		'produkty_zdjecia' => array(
			'product_id' => 'produkt_id',
			'photo_nr' => 'zdjecie_nr',
			'position' => 'zdjecie_pozycja',
			'desc' => 'zdjecie_opis',
		),
    ];

    private $classMapping = [
		'producenci' => '\App\Producer',
		'kategorie' => '\App\Category',
		'produkty' => '\App\Product',
		'klienci' => '\App\Client',
		'produkty_wlasciwosci' => '\App\ProductDetails',
		'produkty_zdjecia' => '\App\ProductPhotos',
    ];

	private $tableMapping = [
		'producenci' => 'producers',
		'kategorie' => 'categories',
		'produkty' => 'clients',
		'klienci' => 'products',
		'produkty_wlasciwosci' => 'product_details',
		'produkty_zdjecia' => 'product_photos',
    ];

    public function refreashSellyKey()
    {
		$jsonCoin = $this->getData('http://api.selly.pl/?api='.config('selly.api_id'));
		$coin = json_decode($jsonCoin);
		$keySelly = hash('sha256', $coin->coin . config('selly.app_key'));
		$jsonToken = $this->getData('http://api.selly.pl/?key='.$keySelly);
		$token = json_decode($jsonToken);
		config(['app.selly_token' => $token->success->token]);
		return $token->success->token;
    }

    public function updateDbWithSellyTables()
    {
		$updateDate = null;
		DB::transaction(function () use(&$updateDate) {
		    $sellyToken = $this->refreashSellyKey();
			$attempts = 3;
			$names = array_keys($this->sellyTables);
			$num = count($names);
			for ($x = 0; $x < $num; $x += 1) {
				try {
					$this->updateTable($names[$x], $sellyToken);
				} catch (\Exception $e) {
					if(0 < $attempts--){
						sleep(3);
						$sellyToken = $this->refreashSellyKey();
						$x -= 1;
						continue;
					}else{
						throw $e;
					}
				}
			}
			$updateDate = $this->saveUpdated();
		});
		return array(
			'updated' => true,
			'date' => $updateDate,
		);
    }

    private function saveUpdated()
    {
		$updateDate = new \DateTime();
		$newPackage = $this->newPackageDate();
		$dbUpdate = \App\DbUpdate::where('id', 1)->first();
		if(!$dbUpdate){
			$dbUpdate = new \App\DbUpdate;
		}
		$dbUpdate->last_update = $updateDate;
		$dbUpdate->new_package = $newPackage;
		$dbUpdate->save();
		return array(
			'date' => $updateDate,
			'newPackage' => $newPackage,
		);
    }

    private function newPackageDate()
    {
		$lastUpdated = MetaData::where('updated_at', '!=', null )->orderBy('updated_at', 'desc')->first();
		if($lastUpdated){
			$last = DB::table($lastUpdated->table_name)->where('updated_at', '!=', null )->orderBy('updated_at', 'desc')->first();
			if($last){
				return $last->updated_at;
			}
			return new \DateTime();
		}
		return new \DateTime();
    }

	private function getData($url) {
		$ch = curl_init();
		$timeout = 30;
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
		$data = curl_exec($ch);
		curl_close($ch);
		return $data;
	}

    private function saveMeta($sellyTableName, $resp)
    {
		$meta = MetaData::where('selly_table_name', $sellyTableName)->first();
		if(!$meta){
			$meta = new MetaData;
			$meta->selly_table_name = $sellyTableName;
			$meta->table_name = $this->tableMapping[$sellyTableName];
		}
		$meta->data = $resp;
		$meta->save();
    }

    private function insert($sellyTableName, $resp)
    {
		$respDecoded = $this->decodeSellyResp($resp, $sellyTableName);
		$this->classMapping[$sellyTableName]::insert($respDecoded);
		$this->saveMeta($sellyTableName, $resp);
    }

    private function checksumCheck($resp)
	{
		$r = explode('.', $resp);
		$respData = $r[0];
		$checksum = $r[1];
		if(hash('sha256', $respData . config('selly.app_key')) !== $checksum){
			throw new \Exception('RESP checksum mismatch');
		}
	}

    private function updateTable($sellyTableName, $sellyToken)
    {
		$resp = $this->getData('http://api.selly.pl/apig?token='.$sellyToken.'&table='.$sellyTableName);
		$this->checksumCheck($resp);
		$metaData = MetaData::where('selly_table_name', $sellyTableName)->first();
		DB::transaction(function () use($sellyTableName, $resp, $metaData) {
			if($metaData){
				$this->update($sellyTableName, $resp, $metaData);
			}else{
				$this->insert($sellyTableName, $resp);
			}
		});
    }

    private function update($sellyTableName, $resp, $metaData)
    {
		$update = array(
			'insert' => array(),
			'update' => array(),
			'delete' => array(),
		);
		$savedResp = $metaData->data;
		if($savedResp !== $resp){
			$decoded = $this->decodeSellyResp($resp, $sellyTableName);
			$savedDecoded = $this->decodeSellyResp($savedResp, $sellyTableName);
			foreach($decoded as $id => $arr){
				if(!array_key_exists($id, $savedDecoded)){
					$update['insert'][$id] = $arr;
				}elseif($diff = array_diff_assoc($arr, $savedDecoded[$id])){
					$update['update'][$id] = $diff;
				}
			}
			foreach($savedDecoded as $id => $arr){
				if(!array_key_exists($id, $decoded)){
					$update['delete'][] = $id;
				}
			}
			$this->saveUpdate($update, $sellyTableName);
			$this->saveMeta($sellyTableName, $resp);
		}
		return 0;
    }

    private function saveUpdate($update, $sellyTableName)
	{
		$tableClass = $this->classMapping[$sellyTableName];
		$tableObj = new $tableClass();
		$tableClass::insert($update['insert']);
		foreach($update['update'] as $id => $diff){
			$tableObj
				->where('id', $id)
				->update($diff);
	    }
//		$tableClass::destroy($update['delete']);
	}

    private function decodeSellyResp($resp, $sellyTableName)
    {
		$jsonString = base64_decode(explode('.', $resp)[0]);
		$arr = json_decode($jsonString, true);
		$respDecoded = array();
		$sellyTable = $this->sellyTables[$sellyTableName];
		$firstKey = array_key_first($sellyTable);
		foreach($arr as $val){
			$respDecoded[$val[$sellyTable[$firstKey]]] = $this->mapSellyColumns($sellyTable, $val);
		}
		return $respDecoded;
    }

    private function mapSellyColumns($sellyTable, $val)
    {
		$mappedVal = array();
		foreach ($sellyTable as $column => $sellyColumn){
			$mappedVal[$column] = $val[$sellyColumn];
		}
		return $mappedVal;
    }
}

