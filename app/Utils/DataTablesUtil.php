<?php

namespace App\Utils;

use App\Utils\DataTablesUtil;

class DataTablesUtil {

    public function currentPage($start, $length)
    {
		return (int)($start/$length) + 1;
    }

    public function collection($currentPage, $params)
    {
		if(!isset($params['resource'])){
			$params['resource'] = $params['model'];
		}
		$resDict = 'App\Http\Resources\DataTables\\';
		$db = isset($params['db'])? $params['db'] : false;
		$builder = ('App\\'.$params['model'])::query();
		if($db && $db['with']){
			$builder->with($db['with']);
		}
		$objs = $builder->paginate($params['length'], ['*'], 'page', $currentPage);
		$params['total'] = $objs->total();
		$c = $resDict . $params['resource'] . 'Collection';
		return new $c($objs, $params);
    }

    public function tree($objs)
    {
		$tree = array();
		foreach($objs as $obj){
			if(!$obj->parent_id){
				$arr = array('text' => $obj->name);
				$nodes = $this->treeFindChildren($obj->id, $objs);
				if($nodes){
					$arr['nodes'] = $nodes;
				}
				$tree[] = $arr;
			}
		}
		return $tree;
    }

    public function treeFindChildren($id, $objs)
	{
		$children = array();
		foreach($objs as $obj){
			if($obj->parent_id === $id){
				$child = array('text' => $obj->name);
				$nodes = $this->treeFindChildren($obj->id, $objs);
				if($nodes){
					$child['nodes'] = $nodes;
				}
				$children[] = $child;
			}
		}
		return $children;
	}

	public function resourcesMetaData($params){
        return [
			'draw' => $params['draw'],
			'recordsTotal' => $params['total'],
			'recordsFiltered' => isset($params['filtered'])? $params['filtered']:$params['total'],
		];
	}

	public function receptionInDays($paymentTerm){
		$now = new \DateTime("now");
		if($paymentTerm > $now){
			$reception = '<b style="color:red" class="reception">'.$paymentTerm->diff($now)->format('%a').' dni</b>';
		}else{
			$reception = '<b style="color:green" class="reception">0 dni</b>';
		}
		return $reception;
	}

}

