<?php

namespace App\Utils;

use App\MetaData;
use Illuminate\Support\Facades\DB;

class ApiUtil {
	
    public function paginate($request)
    {
		return $request->paginate? $request->paginate:config('selly.paginate');
    }

    public function builderAddWhereFromTimestamp($request, $builder)
    {
		$date = new \DateTime();
		$date->setTimestamp($request->from);
		$from = $date->format('Y-m-d h:i:s');
		if($request->from){
			$builder->where(function ($query) use($from) {
                $query->where('created_at', '>=', $from)
                      ->orWhere('updated_at', '>=', $from);
            });
		}
		return $builder;
    }

}

