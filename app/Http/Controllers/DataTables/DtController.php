<?php

namespace App\Http\Controllers\DataTables;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Utils\DataTablesUtil;
use Illuminate\Support\Facades\DB;

class DtController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function dataTables(Request $request)
    {
		$dataTablesUtil = new DataTablesUtil();
		$params = $request->all();
		$currentPage = $dataTablesUtil->currentPage($params['start'], $params['length']);
		return $dataTablesUtil->collection($currentPage, $params);
	}

    public function categories()
    {
		$dataTablesUtil = new DataTablesUtil();
		$objs = DB::table('categories')->select('*')->get();
		return response()->json([
			'tree' => $dataTablesUtil->tree($objs)],
		);
	}

    public function datetimepicker(Request $request)
    {
		$model = 'App\\'.$request->model;
		$obj = $model::findOrFail($request->id);
		if($obj){
			$obj->payment_term = $request->date;
			$obj->save();
			$dataTablesUtil = new DataTablesUtil();
			$reception = $dataTablesUtil->receptionInDays(new \DateTime($request->date));
			return response()->json([
				'ok' => true,
				'reception' => $reception,
			]);
		}
	}
}
