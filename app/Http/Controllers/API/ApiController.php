<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApiController extends Controller
{
    public function newpackage(Request $request)
    {
		return response()->json([
			'newPackage' => config('selly.newPackage'),
		]);
    }
}
