<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Order as OrderResource;
use App\Order;
use App\Http\Resources\OrderCollection;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
		$apiUtil = new \App\Utils\ApiUtil();
		$paginate = $apiUtil->paginate($request);
        $builder = Order::query();
		if($request->from){
			$builder = $apiUtil->builderAddWhereFromTimestamp($request, $builder);
		}
		$orders = $builder->with('products')->paginate($paginate);
		return new OrderCollection($orders);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
		return new OrderResource($order);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$order = new Order;
		DB::transaction(function () use(&$order, $request) {
			$order->comment = $request->comment;
			$order->sent = $request->sent;
			$order->payment_term = $request->payment_term;
			$order->client_id = $request->client_id;
			$order->save();
			foreach ($request->products as $product){
				$productObj = \App\Product::find($product['id']);
				$order->products()->save($productObj, $product['pivot']);
			}
		});
		return response()->json(new OrderResource($order), 201);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		$order = Order::with(['products'])->find($id);
		$products = array();
		foreach ($request->products as $product){
			$products[$product['id']] = $product;
		}
		DB::transaction(function () use(&$order, $products, $request) {
			if($request->comment){
				$order->comment = $request->comment;
			}
			if($request->sent){
				$order->sent = $request->sent;
			}
			if($request->payment_term){
				$order->payment_term = $request->payment_term;
			}
			if($request->client_id){
				$order->client_id = $request->client_id;
			}
			$order->save();
			foreach ($order->products as $productObj){
				$id = $productObj->id;
				if(array_key_exists($id, $products)){
					$order->products()->updateExistingPivot($id, $products[$id]['pivot']);
					unset($products[$id]);
				}
			}
			foreach ($products as $product){
				$productObj = \App\Product::find($product['id']);
				$order->products()->save($productObj, $product['pivot']);
			}
		});
		return response()->json(new OrderResource($order), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        $order->delete();
        return response()->json(null, 204);
    }

}
