<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UtilController extends Controller
{
    public function _clearcache()
    {
		\Artisan::call('cache:clear');
		\Artisan::call('config:clear');
		\Artisan::call('config:cache');
		\Artisan::call('view:clear');
		\Artisan::call('route:cache');
		return "Cleared!";
    }

    public function _migr()
    {
		\Artisan::call('migrate');
		return "Migrated!";
    }

}
