<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Utils\CardsDbUtil;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

//    public function welcome()
//    {
//		return view('welcome');
//    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
		return view('home', [
			'admin' => 'admin' === Auth::user()->name,
		]);
    }

    public function updatedb()
    {
		$cardsDbUtil = new CardsDbUtil();
		$info = $cardsDbUtil->updateDbWithSellyTables();
		return response()->json(array(
			'updated' => $info['updated'],
			'date' => $info['date']['date']->format('Y-m-d H:i:s'),
			'newPackage' => $info['date']['newPackage'],
		));
    }

}
