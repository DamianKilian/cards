<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Product extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
		$product = parent::toArray($request);
		$details = $this->details;
		unset($product['details']);
		$product['vat'] = $details->vat;
		$product['net_price'] = bcmul(bcdiv((100 - $details->vat).'', '100', 2), $product['price'], 2);
		$product['photos'] = array();
		foreach ($this->photos as $photo){
			$product['photos'][] = 'https://hurtowniakartekonline.pl/'.$details->photo_dir.$photo->photo_nr.'.jpg';
		}
        return $product;
    }
}
