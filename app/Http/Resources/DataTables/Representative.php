<?php

namespace App\Http\Resources\DataTables;

use Illuminate\Http\Resources\Json\JsonResource;

class Representative extends JsonResource
{
    public function toArray($request)
    {
		$clients = array();
		foreach($this->clients as $client){
			$clients[] = [
				'client' => $client->name,
			];
		}
		return [
			'DT_RowId' => 'row_'.$this->id,
			'id' => $this->id,
			'name' => $this->name,
			'clients' => $clients,
		];
    }
}
