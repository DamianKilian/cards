<?php

namespace App\Http\Resources\DataTables;

use Illuminate\Http\Resources\Json\JsonResource;

class Product extends JsonResource
{
    public function toArray($request)
    {
		$photosString = '';
		foreach ($this->photos as $photo){
			 $photosString .= '<img src="https://hurtowniakartekonline.pl/'.$this->details->photo_dir.$photo->photo_nr.'_min.jpg"> ';
		}
		return [
			'name'      => $this->name,
			'category'  => $this->category->name,
			'producer'  => $this->producer? $this->producer->name:'',
			'desc'      => $this->desc,
			'photos'    => $photosString,
			'updated_at'=> $this->updated_at? $this->updated_at->format('Y-m-d H:i:s'):'',
			'price'     => $this->price,
			'visible'   => $this->visible,
		];
    }
}
