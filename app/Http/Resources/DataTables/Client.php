<?php

namespace App\Http\Resources\DataTables;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Utils\DataTablesUtil;

class Client extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
		$dataTablesUtil = new DataTablesUtil();
		$reception = $dataTablesUtil->receptionInDays(new \DateTime($this->payment_term));
		return [
			'DT_RowId' => $this->id,
			'id'   => $this->id,
			'name' => $this->name,
			'rabate'   => $this->rabate,
			'address' => $this->city.', '.$this->address,
			'payment_term' => null,
			'payment_reception' => $reception,
			'payment_termDate' => $this->payment_term,
		];
    }
}
