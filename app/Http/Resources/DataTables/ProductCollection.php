<?php

namespace App\Http\Resources\DataTables;

use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Utils\DataTablesUtil;

class ProductCollection extends ResourceCollection
{
	public $collects = 'App\Http\Resources\DataTables\Product';

    public function __construct($resource, $params)
    {
        $this->params = $params;
        parent::__construct($resource);
    }

    public function toArray($request)
    {
        return [
            'data' => $this->collection,
        ];
    }

    public function with($request)
    {
		$dataTablesUtil = new DataTablesUtil();
        return $dataTablesUtil->resourcesMetaData($this->params);
    }
}
