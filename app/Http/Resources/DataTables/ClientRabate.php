<?php

namespace App\Http\Resources\DataTables;

use Illuminate\Http\Resources\Json\JsonResource;

class ClientRabate extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
		$producerRabates = array();
		foreach($this->producerRabates as $producerRabate){
			$producerRabates[] = [
				'producer' => $producerRabate->Producer? $producerRabate->Producer->name:'',
				'rabate' => $producerRabate->producer_rabate,
			];
		}
		return [
			'DT_RowId' => 'row_'.$this->id,
			'client' => $this->name,
			'rabate' => $this->rabate,
			'client_surname' => $this->surname,
			'producerRabates' => $producerRabates,
		];
    }
}
