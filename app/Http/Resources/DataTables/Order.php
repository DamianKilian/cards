<?php

namespace App\Http\Resources\DataTables;

use Illuminate\Http\Resources\Json\JsonResource;

class Order extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
		$products = array();
		$price_products = 0;
		foreach($this->products as $product){
			$price_sum = bcmul($product->price, $product->pivot->number, 2);
			$products[] = [
				'name' => $product->name,
				'desc' => $product->desc,
				'price' => $product->price,
				'price_sum' => $price_sum,
				'number' => $product->pivot->number,
				'comment' => $product->pivot->comment,
			];
			$price_products = bcadd($price_products, $price_sum, 2);
		}
		return [
			'DT_RowId' => 'row_'.$this->id,
			'client' => $this->client->name,
			'client_surname' => $this->client->surname,
			'comment' => $this->comment,
			'created_at' => $this->created_at? $this->created_at->format('Y-m-d H:i:s'):'',
			'updated_at' => $this->updated_at? $this->updated_at->format('Y-m-d H:i:s'):'',
			'payment_term' => $this->payment_term,
			'sent' => $this->sent,
			'pdf' => $this->pdf,
			'products' => $products,
			'price_products' => $price_products,
		];
    }
}
