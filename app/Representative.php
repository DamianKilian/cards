<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Representative extends Model
{
    /**
     * Get the clients for the blog post.
     */
    public function clients()
    {
        return $this->hasMany('App\Client');
    }
}
