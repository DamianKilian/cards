<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
	protected $fillable = ['comment', 'client_id'];
    public function products()
    {
        return $this->belongsToMany('App\Product')->withPivot(['number', 'comment']);
    }

    public function client()
    {
        return $this->belongsTo('App\Client');
    }
}
