<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DbUpdate extends Model
{
	public $timestamps = false;
	protected $fillable = ['id', 'last_update'];
}
