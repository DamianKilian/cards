<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProducerRabate extends Model
{
    /**
     * Get the client that owns the comment.
     */
    public function Client()
    {
        return $this->belongsTo('App\Client');
    }

    /**
     * Get the producer that owns the comment.
     */
    public function Producer()
    {
        return $this->belongsTo('App\Producer');
    }
}
