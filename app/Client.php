<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    /**
     * Get the representative that owns the comment.
     */
    public function representative()
    {
        return $this->belongsTo('App\Representative');
    }

    public function producerRabates()
    {
        return $this->hasMany('App\ProducerRabate');
    }
}
