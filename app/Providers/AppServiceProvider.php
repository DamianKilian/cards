<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use App\DbUpdate;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
		$dbUpdate = DbUpdate::find(1);
		if($dbUpdate){
			$lastUpdate = $dbUpdate->last_update;
			$newPackage = $dbUpdate->new_package;
		}else{
			$lastUpdate = 'empty';
			$newPackage = 'empty';
		}
		config([
			'selly.lastUpdate' => $lastUpdate,
			'selly.newPackage' => $newPackage,
		]);
		View::share('lastUpdate', $lastUpdate);
		View::share('newPackage', $newPackage);
    }
}
