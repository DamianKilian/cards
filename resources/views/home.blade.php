@extends('layouts.app')

@push('css')
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css"/>
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.css"/>
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.min.css"/>
	<link href="{{ asset('css/bootstrap-treeview.css') }}" rel="stylesheet">
@endpush

@push('scripts')
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
	<script src="{{ asset('js/bootstrap-datetimepicker.min.js') }}" ></script>
	<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.js"></script>
	<script src="{{ asset('js/dataTablesHandler.js') }}" ></script>
	<script src="{{ asset('js/bootstrap-treeview.js') }}" ></script>
@endpush

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!<br><br>

					<nav id="dt-nav">
						<div class="nav nav-tabs" id="nav-tab" role="tablist">
							<a class="nav-item nav-link active" data-table="orders" data-toggle="tab" href="#nav-orders" role="tab" aria-controls="nav-home" aria-selected="true">Zamówienia</a>
							<a class="nav-item nav-link" data-table="products" data-toggle="tab" href="#nav-products" role="tab" aria-controls="nav-profile" aria-selected="false">Towary</a>
							<a class="nav-item nav-link tree" data-table="categories" data-toggle="tab" href="#nav-categories" role="tab" aria-controls="nav-contact" aria-selected="false">Kategorie</a>
							<a class="nav-item nav-link" data-table="rabates" data-toggle="tab" href="#nav-rabates" role="tab" aria-controls="nav-home" aria-selected="true">Rabaty</a>
							<a class="nav-item nav-link" data-table="producers" data-toggle="tab" href="#nav-producers" role="tab" aria-controls="nav-profile" aria-selected="false">Producenci</a>
							<a class="nav-item nav-link" data-table="clients" data-toggle="tab" href="#nav-clients" role="tab" aria-controls="nav-contact" aria-selected="false">Klienci</a>
							<a class="nav-item nav-link" data-table="representatives" data-toggle="tab" href="#nav-representatives" role="tab" aria-controls="nav-contact" aria-selected="false">Przedstawiciele</a>
						</div>
					</nav>
					<div class="tab-content" id="nav-tabContent" data-url="{{ url('/') }}">
						<div class="tab-pane fade show active" id="nav-orders" role="tabpanel" aria-labelledby="orders">
							<br>
							<table id="dt-orders" class="display" style="width:100%">
								<thead>
									<tr>
										<th></th>
										<th>Klient</th>
										<th>Komentarz</th>
										<th>Utworzono</th>
										<th>Aktualizowano</th>
										<th>Termin płatności</th>
										<th>Wysłano</th>
										<th>Pdf</th>
									</tr>
								</thead>
							</table>
						</div>
						<div class="tab-pane fade" id="nav-products" role="tabpanel" aria-labelledby="products">
							<br>
							<table id="dt-products" class="display" style="width:100%">
								<thead>
									<tr>
										<th>Nazwa</th>
										<th>Kategoria</th>
										<th>Producent</th>
										<th>Opis</th>
										<th>Zdjęcia</th>
										<th>Aktualizowano</th>
										<th>Cena</th>
										<th>Widoczność</th>
									</tr>
								</thead>
							</table>
						</div>
						<div class="tab-pane fade" id="nav-categories" role="tabpanel" aria-labelledby="categories">
							<br>
							<div id="dt-categories">
								<form class="treeSearch d-none">
									<div class="form-group">
										<label for="input-search" class="sr-only">Szukaj:</label>
										<input type="input" class="form-control" id="searchCatTree" placeholder="Type to search..." value="">
									</div>
									<button type="button" class="btn btn-success" id="btn-search">Search</button>
									<button type="button" class="btn btn-default" id="btn-clear-search">Clear</button>
									<br><br>
								</form>
								<div id="tree"></div>
								<div class="text-center loader">
									<div class="spinner-border text-primary" role="status">
										<span class="sr-only">Loading...</span>
									</div>
								</div>
							</div>
						</div>
						<div class="tab-pane fade" id="nav-rabates" role="tabpanel" aria-labelledby="rabates">
							<br>
							<table id="dt-rabates" class="display" style="width:100%">
								<thead>
									<tr>
										<th></th>
										<th>Klient</th>
										<th>Rabat ogólny</th>
									</tr>
								</thead>
							</table>
						</div>
						<div class="tab-pane fade" id="nav-producers" role="tabpanel" aria-labelledby="producers">
							<br>
							<table id="dt-producers" class="display" style="width:100%">
								<thead>
									<tr>
										<th>Id</th>
										<th>Nazwa</th>
									</tr>
								</thead>
							</table>
						</div>
						<div class="tab-pane fade" id="nav-clients" role="tabpanel" aria-labelledby="clients">
							<br>
							<table id="dt-clients" class="display" style="width:100%">
								<thead>
									<tr>
										<th>Id</th>
										<th>Nazwa</th>
										<th>Adres</th>
										<th>Rabat</th>
										<th>Termin płatności</th>
										<th>Pobór płatności</th>
									</tr>
								</thead>
							</table>
						</div>
						<div class="tab-pane fade" id="nav-representatives" role="tabpanel" aria-labelledby="representatives">
							<br>
							<table id="dt-representatives" class="display" style="width:100%">
								<thead>
									<tr>
										<th></th>
										<th>Id</th>
										<th>Nazwa</th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
